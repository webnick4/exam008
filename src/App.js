import React, {Component, Fragment} from 'react';
import './App.css';
import {NavLink, Route, Switch} from "react-router-dom";
import QuotesList from "./containers/QuotesList/QuotesList";
import QuotesAdd from "./containers/QuotesAdd/QuotesAdd";
import QuoteFull from "./containers/QuoteFull/QuoteFull";
import StarWars from "./containers/Quotes/StarWars/StarWars";
import FamousPeople from "./containers/Quotes/FamousPeople/FamousPeople";
import Saying from "./containers/Quotes/Saying/Saying";
import Humour from "./containers/Quotes/Humour/Humour";
import Motivational from "./containers/Quotes/Motivational/Motivational";



class App extends Component {
  render() {
    return (
      <Fragment>
        <nav className='App-nav'>
          <ul>
            <li><NavLink to='/' exact>Quotes</NavLink></li>
            <li><NavLink to='/add-quote' exact>Submit new Quotes</NavLink></li>
          </ul>
        </nav>
        <Switch>
          <Route path='/' exact component={QuotesList} />
          <Route path='/add-quote' component={QuotesAdd} />
          <Route path='/quotes/:id' component={QuoteFull} />
          <Route path='/quotes/star-wars/:id' component={StarWars} />
          <Route path='/quotes/famous-people/:id' component={FamousPeople} />
          <Route path='/quotes/saying/:id' component={Saying} />
          <Route path='/quotes/humour/:id' component={Humour} />
          <Route path='/quotes/motivational/:id' component={Motivational} />
          <Route render={() => <h1>Not Found</h1>} />
        </Switch>
      </Fragment>
    );
  }
}

export default App;


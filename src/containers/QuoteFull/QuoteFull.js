import React, {Component} from 'react';
import './QuoteFull.css';
import axios from 'axios';

class QuoteFull extends Component {
  state = {
    loadedNote: null
  };

  componentDidMount() {
    const id = this.props.match.params.id;

    axios.get(`/quotes/${id}.json`).then(response => {
      this.setState({loadedNote: response.data});
    })
  }

  render() {
    if (this.state.loadedNote) {
      return (
        <div className="QuoteFull">
          <h1>{this.state.loadedNote.category}</h1>
          <p>{this.state.loadedNote.text}</p>
          <p>{this.state.loadedNote.author}</p>
        </div>
      );
    } else {
      return <p>Loading...</p>;
    }
  }
}

export default QuoteFull;
import React, {Component, Fragment} from 'react';
// import './Saying.css';
import {Link} from "react-router-dom";
import axios from 'axios';
import QuoteShortDesc from "../../../components/Quotes/QuoteShortDesc/QuoteShortDesc";


class Saying extends Component {
  state = {
    quotes: []
  };


  componentDidMount() {
    if (this.props.match.path === '/quotes/saying') {
      axios.get('/quotes.json').then(response => {
        response.data ? this.setState({quotes: response.data}) : this.setState({quotes: []});
      });
    }
  }

  quoteDeleteHandler = (id) => {
    axios.delete(`/quotes/${id}.json`).then(() => {
      this.props.history.push('/quote');
    });
  };

  render() {
    return (
      <Fragment>
        <nav className='App-nav'>
          <ul>
            <li><Link to='/'>All</Link></li>
            <li><Link to='/quotes/star-wars/' >Start Wars</Link></li>
            <li><Link to='/quotes/famous-people' >Famous People</Link></li>
            <li><Link to='/quotes/saying' >Saying</Link></li>
            <li><Link to='/quotes/humour' >Humour</Link></li>
            <li><Link to='/quotes/motivational' >Motivational</Link></li>
          </ul>
        </nav>

        <div className='QuotesList'>
          {
            Object.keys(this.state.quotes).map(quote => (
              <QuoteShortDesc
                key={quote}
                category={this.state.quotes[quote].category}
                author={this.state.quotes[quote].author}
                text={this.state.quotes[quote].text}
                id={quote}
                deleted={() => this.quoteDeleteHandler(quote)}
              />
            ))}
        </div>
      </Fragment>
    )
  }
}

export default Saying;
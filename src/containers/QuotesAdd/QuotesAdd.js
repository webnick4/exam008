import React, {Component, Fragment} from 'react';
import './QuotesAdd.css';
import axios from 'axios';

class QuotesAdd extends Component {
  state = {
    quote: {
      category: '',
      author: '',
      text: ''
    },
    loading: false
  };

  quoteCreateHandler = event => {
    event.preventDefault();

    this.setState({loading: true});

    axios.post('/quotes.json', this.state.quote).then(() => {
      this.setState({loading: false});
      this.props.history.replace('/');
    });
  };

  quoteValueChanged = event => {
    event.persist();
    this.setState(prevState => {
      return {
        quote: {...prevState.quote, [event.target.name]: event.target.value}
      };
    });
  };


  render() {
    return (
      <Fragment>
        <h3>Submit new category</h3>
        <form className='QuoteAdd'>
          <label htmlFor="category">Category</label>
          <p>
            <select name='category' onChange={this.quoteValueChanged}>
              <option value="star-wars" >Star Wars</option>
              <option value="famous-people" >Famous People</option>
              <option value="saying" >Saying</option>
              <option value="humour" >Humour</option>
              <option value="motivational" >Motivational</option>
            </select>
          </p>

          <label htmlFor="author">Author</label>
          <input type='text' name='author' placeholder='Enter author'
                 value={this.state.quote.author} onChange={this.quoteValueChanged}
          />
          <textarea name='text' placeholder='Enter your quote...'
                    value={this.state.quote.text} onChange={this.quoteValueChanged}
          />
          <button onClick={this.quoteCreateHandler}>Save</button>
        </form>
      </Fragment>
    )
  }
}

export default QuotesAdd;
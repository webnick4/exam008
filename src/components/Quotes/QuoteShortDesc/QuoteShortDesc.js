import React from 'react';
import './QuoteShortDesc.css';
import {Link} from "react-router-dom";

const QuoteShortDesc = props => (
  <div className='NoteShortDesc' key={props}>
    <h4>
      <Link to={'/quotes/' + props.id}>{props.category}</Link>
      <button onClick={props.deleted}>Delete</button>
    </h4>
    <p>"{props.text}"</p>
    <p>....{props.author}</p>
  </div>
);

export default QuoteShortDesc;